#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Toy webservice for storing files remotely
# Copyright (C) 2020 Clément Schreiner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from hashlib import sha1
import mimetypes
import os
from typing import Optional

from attr import attrs, attrib, asdict
from attr.validators import instance_of, optional
from json import dumps, loads
from sanic import Sanic
from sanic.request import File, Request
from sanic.response import json

app = Sanic("Mux Sharing Server")


@attrs(kw_only=True)
class UploadedFile:
    data: bytes = attrib(validator=instance_of(bytes))
    name: str = attrib(validator=instance_of(str))
    mime_type: str = attrib(validator=instance_of(str))


@attrs(kw_only=True)
class StoredFile:
    mime_type: str = attrib(validator=instance_of(str))
    name: str = attrib(validator=instance_of(str))
    path: str = attrib(validator=instance_of(str))
    size: int = attrib(validator=instance_of(int))
    # TODO: add validator sha1sum size (should be 20 chars)
    sha1sum: str = attrib(validator=instance_of(str))


@attrs(kw_only=True)
class UploadResult:
    # TODO: status should be an enum
    status: str = attrib(validator=instance_of(str))
    stored_file: Optional[StoredFile] = attrib(
        validator=optional(instance_of(StoredFile)),
        default=None
    )


class Store:
    def __init__(self, db_path):
        self.db_path = db_path
        self.store = {}
        self.load()

    def __iter__(self):
        return self.store.__iter__()

    def __contains__(self, sha1sum):
        return sha1sum in self.store

    def load(self):
        with open(self.db_path, 'r') as f:
            file_data = f.read()
        if len(file_data) == 0:
            return

        for file_dict in loads(file_data):
            self.store[file_dict['sha1sum']] = file_dict
            print(file_dict)

    def add(self, stored_file: StoredFile):
        stored_file_dict = asdict(stored_file)
        self.store[stored_file_dict['sha1sum']] = stored_file_dict

    def get(self, sha1sum: str):
        # TODO: serialize into StoredFile
        return self.store.get(sha1sum)

    def save(self):
        store_list = []
        for _, v in self.store.items():
            store_list.append(v)
        store_json = dumps(store_list)

        with open(self.db_path, 'w') as f:
           f.write(store_json)


def store_file(uploaded_file: UploadedFile, sha1sum: str) -> StoredFile:
    # TODO: move this to Store
    # TODO: make this async
    fileext = mimetypes.guess_extension(uploaded_file.mime_type)
    filepath = os.path.join(app.config['UPLOAD_DIR'], sha1sum + fileext)

    with open(filepath, 'wb') as f:
        f.write(uploaded_file.data)

    stored_file = StoredFile(
        mime_type=uploaded_file.mime_type,
        name=uploaded_file.name,
        path=filepath,
        sha1sum=sha1sum,
        size=len(uploaded_file.data),
    )

    return stored_file


def handle_upload(uploaded_file: UploadedFile) -> UploadResult:
    # TODO: make this async
    if len(uploaded_file.data) > app.config.get('UPLOAD_MAX_SIZE'):
        return UploadResult(
            status="too-large"
        )
    store = Store(app.config.get('STORE_PATH'))

    sha1sum = sha1(uploaded_file.data).hexdigest()
    if sha1sum in store:
        stored_file_dict = store.get(sha1sum)
        stored_file = StoredFile(
            mime_type=stored_file_dict['mime_type'],
            name=stored_file_dict['name'],
            path=stored_file_dict['path'],
            size=stored_file_dict['size'],
            sha1sum=stored_file_dict['sha1sum'],
        )
        status = 'file-exists'
    else:
        stored_file = store_file(uploaded_file, sha1sum)
        store.add(stored_file)
        store.save()
        status = 'uploaded'

    result = UploadResult(
        status=status,
        stored_file=stored_file,
    )

    return result


@app.route('/')
async def test(request: Request):
    response = {
        'hello': 'world',
    }
    if request.json:
        response['got_your']: 'json'
    return json(response)


def error(reason=None):
    result = {'reason': reason}
    return json(result, status=400)


@app.route('/upload', methods=['POST'])
async def upload_file(request: Request):
    file_field: File = request.files.get('file')
    if file_field is None:
        return error(reason='No file found')

    name_field = request.form.get('title', file_field)
    uploaded_file = UploadedFile(
        data=file_field.body,
        name=name_field,
        mime_type=file_field.type,
    )
    result = handle_upload(uploaded_file)
    return json(asdict(result))


@app.route('/uploads', methods=['GET'])
async def get_uploads(request: Request):
    results = []
    store = Store(app.config.get('STORE_PATH'))
    for stored_file in store:
        results.append(stored_file)
    return json(results)

if __name__ == '__main__':
    app.config['STORE_PATH'] = 'store.json'
    app.config['UPLOAD_DIR'] = './uploads'
    app.config['UPLOAD_MAX_SIZE'] = 10*1024*1024

    if not os.path.exists(app.config.UPLOAD_DIR):
        os.makedirs(app.config.UPLOAD_DIR)

    app.run(host='0.0.0.0', port=8000)
